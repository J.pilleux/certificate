package cert

import (
	"testing"
	"unicode"
)

func isUpperString(str string) bool {
	for _, s := range str {
		if !unicode.IsUpper(s) && unicode.IsLetter(s) {
			return false
		}
	}
	return true
}

func TestValidCertData(t *testing.T) {
	c, err := New("Golang", "Billy", "2022-07-22")
	if err != nil {
		t.Errorf("Cert data should be valid. err=%v", err)
	}

	if c == nil {
		t.Errorf("Cert should be a valide reference. got=nil")
	}

	if c.Course != "GOLANG COURSE" {
		t.Errorf("Course name is not valid. expected='GOLANG COURS', got=%v\n", c.Course)
	}
}

func TestCourseEmptyValue(t *testing.T) {
	_, err := New("", "Billy", "2022-07-22")
	if err == nil {
		t.Error("Error should be returned on an empty course")
	}
}

func TestCourseTooLong(t *testing.T) {
	course := "TRASTNSRTNTSNSTNRSTNSTNSTNRSTNRSTNRSNRTNRSTNRSTNRSRNTSNSTNRSTNRSTNRNT"
	_, err := New(course, "Billy", "2022-07-22")
	if err == nil {
		t.Errorf("Error should be returned on a too long course name (course='%s')", course)
	}
}

func TestNameIsUpperCase(t *testing.T) {
	c, _ := New("Golang", "Billy", "2022-07-22")
	if !isUpperString(c.Name) {
		t.Errorf("The name '%s' should be in upper case", c.Name)
	}
}

func TestNameEmptyValue(t *testing.T) {
	_, err := New("Golang", "", "2022-07-22")
	if err == nil {
		t.Error("Error should be returned on an empty name")
	}
}

func TestNameTooLong(t *testing.T) {
	name := "This name is way too long to be used as a valid name in the certificate generator"
	_, err := New("Golang", name, "2022-07-22")
	if err == nil {
		t.Errorf("Error should be returned on a too long name (name='%s')", name)
	}
}

func TestDateFormatted(t *testing.T) {
	expected := "Date: 22/07/2022"
	c, _ := New("Golang", "Billy", "2022-07-22")
	if c.LabelDate != expected {
		t.Errorf("The date is not correctly formatted: expected='%s', got='%s'", expected, c.LabelDate)
	}
}
