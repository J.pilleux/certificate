package cert

import (
	"fmt"
	"strings"
	"time"
)

const maxNameLen = 30

type Cert struct {
	Course string
	Name   string
	Date   time.Time

	LabelTitle         string
	LabelCompletion    string
	LabelPresented     string
	LabelParticipation string
	LabelDate          string
}

type Saver interface {
	Save(c Cert) error
}

func New(course, name, date string) (*Cert, error) {
	c, err := validateCourse(course)
	if err != nil {
		return nil, err
	}

	n, err := validateName(name)
	if err != nil {
		return nil, err
	}

	d, err := validateDate(date)
	if err != nil {
		return nil, err
	}

	cert := &Cert{
		Course:             c,
		Name:               n,
		LabelTitle:         fmt.Sprintf("%v Certificate - %v", c, n),
		LabelCompletion:    "Certificate of Completion",
		LabelPresented:     "This Certificate is Presented To",
		LabelParticipation: fmt.Sprintf("For participation in tho %v", c),
		LabelDate:          fmt.Sprintf("Date: %v", d.Format("02/01/2006")),
	}

	return cert, nil
}

func validateName(name string) (string, error) {
	n, err := validateStr(name, maxNameLen)
	if err != nil {
		return "", err
	}

	return strings.ToUpper(n), nil
}

func validateCourse(course string) (string, error) {
	c, err := validateStr(course, maxNameLen)
	if err != nil {
		return "", err
	}
	if !strings.HasSuffix(c, " course") {
		c += " course"
	}

	return strings.ToTitle(c), nil
}

func validateStr(str string, maxLen int) (string, error) {
	c := strings.TrimSpace(str)
	if len(c) <= 0 {
		return c, fmt.Errorf("invalid string. got='%s', len=%d", c, len(c))
	} else if len(c) >= maxLen {
		return c, fmt.Errorf("the course name is too long. len='%d', max='%d'", len(c), maxLen)
	}
	return c, nil
}

func validateDate(date string) (time.Time, error) {
	t, err := time.Parse("2006-01-02", date)
	if err != nil {
		return t, err
	}

	return t, nil
}
