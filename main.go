package main

import (
	"flag"
	"fmt"
	"os"
	"udemy/certificate/cert"
	"udemy/certificate/html"
	"udemy/certificate/pdf"
)

func main() {
	outputType := flag.String("type", "pdf", "Output type of the certificate")
	certFilePath := flag.String("file", "students.csv", "File path of the csv containing the certificate data")
	flag.Parse()

	var saver cert.Saver
	var err error

	switch *outputType {
	case "html":
		saver, err = html.New("output")
	case "pdf":
		saver, err = pdf.New("output")
	default:
		err = fmt.Errorf("unknown output type. got=%v", *outputType)
	}

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: could not create generator, %v\n", err)
		os.Exit(1)
	}

	certs, err := cert.ParseCSV(*certFilePath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: could not parse the CSV file '%v'\n", err)
		os.Exit(1)
	}

	for _, c := range certs {
		if err != nil {
			fmt.Printf("Error during certification creation: %v\n", err)
			os.Exit(1)
		}

		saver.Save(*c)
	}
}
